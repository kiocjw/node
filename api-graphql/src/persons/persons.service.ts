import { Model } from 'mongoose';
import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Person, PersonDocument } from './persons.schema';
import { PersonInput } from './inputs/person.input';
import { IServiceGetPersonResponse } from 'src/interfaces/person/service-get-person-response.interface';
import { ClientProxy } from '@nestjs/microservices';
import { IPerson } from 'src/interfaces/person/person.interface';
import { encrypt } from 'src/encryption';

@Injectable()
export class PersonsService {
  constructor(
    @InjectModel(Person.name) private personModel: Model<PersonDocument>,
    @Inject('PERSON_SERVICE') private readonly userServiceClient: ClientProxy
  ) {}


  async findOne(ID: string): Promise<IPerson | undefined> {
    const createPersonResponse: IServiceGetPersonResponse = await this.userServiceClient
    .send('get_person', encrypt(ID))
    .toPromise();
    if (createPersonResponse.status !== HttpStatus.OK) {
      throw new HttpException(
        {
          message: createPersonResponse.message,
          data: null,
        },
        createPersonResponse.status,
      );
    }
    const jwtPerson = createPersonResponse.person;
    return jwtPerson;
  }

  async findAll(): Promise<Person[]> {
    return this.personModel.find().exec();
  }
}
