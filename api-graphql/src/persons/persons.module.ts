import { Module } from '@nestjs/common';
import { PersonsResolver } from './persons.resolver';
import { Person, PersonSchema } from './persons.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { PersonsService } from './persons.service';
import { ConfigService } from 'src/services/config/config.service';
import { ClientProxyFactory } from '@nestjs/microservices';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Person.name, schema: PersonSchema }]),
  ],
  providers: [PersonsResolver, PersonsService,
    ConfigService,
    {
      provide: 'PERSON_SERVICE',
      useFactory: (configService: ConfigService) => {
        const userServiceOptions = configService.get('personService');
        return ClientProxyFactory.create(userServiceOptions);
      },
      inject: [ConfigService],
    },],
  exports: [PersonsService],
})
export class PersonsModule {}
