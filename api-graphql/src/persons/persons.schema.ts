import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { fieldEncryption } from 'mongoose-field-encryption';
import { KEY } from './../encryption';

export type PersonDocument = Person & Document;

@Schema()
export class Person {
  @Prop()
  firstName: string;

  @Prop()
  lastName: string;

  @Prop() // ({ unique: true })// Unique is not working with fieldEncryption
  passport: string;

  @Prop()
  email: string;

  @Prop()
  address: string;
}

export const PersonSchema = SchemaFactory.createForClass(Person);
PersonSchema.plugin(fieldEncryption, {
  fields: ['firstName', 'lastName', 'passport', 'email', 'address'],
  secret: KEY,
});
