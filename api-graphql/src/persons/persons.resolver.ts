import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { HttpException, HttpStatus, Inject, UnauthorizedException, UseGuards } from '@nestjs/common';
import { PersonsService } from './persons.service';
import { PersonType } from './objects/person.object';
import { PersonInput } from './inputs/person.input';
import { decrypt, encrypt } from './../encryption';
import { CurrentUser } from './../auth/current-user.decorator';
import { User } from './objects/user.object';
import { GqlAuthGuard } from './../auth/guards/graphql.guard';
import { ClientProxy } from '@nestjs/microservices';
import { IServicePersonCreateResponse as IServiceCreatePersonURLResponse } from 'src/interfaces/person/service-create-person-url-response-interface';
import { IServiceGetPersonResponse } from 'src/interfaces/person/service-get-person-response.interface';
import { collectComment } from 'graphql-tools';

export const HOSTNAME =
  'https://ts-api-xvdmxzkhuq-ue.a.run.app';

@Resolver()
export class PersonsResolver {
  constructor(private readonly personsService: PersonsService,
    @Inject('PERSON_SERVICE') private readonly userServiceClient: ClientProxy) {}

  @Query(() => String)
  async index() {
    return 'Index';
  }

  /*
  @Query(returns => String)
  @UseGuards(GqlAuthGuard)
  whoAmI(@CurrentUser() user: User) {
    return user.userId;
  }s

  @Mutation(() => PersonType)
  @UseGuards(GqlAuthGuard)
  async createPerson(@Args('input') input: PersonInput) {
    return this.personsService.create(input);
  }

  @Query(() => [PersonType])
  async persons() {
    return this.personsService.findAll();
  }
  */
  @Mutation(() => String)
  async createPersonURL(@Args('input') input: PersonInput) {

    const createPersonResponse: IServiceCreatePersonURLResponse = await this.userServiceClient
    .send('create_person_url', input)
    .toPromise();
    if (createPersonResponse.status !== HttpStatus.CREATED) {
      throw new HttpException(
        {
          message: createPersonResponse.message,
          data: null,
          errors: createPersonResponse.errors,
        },
        createPersonResponse.status,
      );
    }

    const code = createPersonResponse.code;
    if (!code) {
      throw new Error('Passport is already exist!');
    }
    return (
      encodeURI(`${HOSTNAME}/graphql?query={getPerson(code:"`) +
      `${encrypt(code)}` +
      encodeURI(`"){id,firstName,lastName,passport}}`)
    );
  }

  @Query(() => PersonType)
  async getPerson(@Args('code') code: string) {
    try {
      const createPersonResponse: IServiceGetPersonResponse = await this.userServiceClient
      .send('get_person', code)
      .toPromise();
      if (createPersonResponse.status !== HttpStatus.OK) {
        throw new HttpException(
          {
            message: createPersonResponse.message,
            data: null,
          },
          createPersonResponse.status,
        );
      }
      let person = createPersonResponse.person;
      person.id = decrypt(code);
      console.log("getPerson");
      console.log(person);
      if (!person) {
        throw new Error('Invalid request!');
      }
      else
      {
        return person;
      }
    } catch (error) {
      return {
        id: 'null',
        firstName: 'null',
        lastName: 'null',
        passport: 'null',
      };
    }
  }

  @Query(() => String)
  @UseGuards(GqlAuthGuard)
  async getPersonURLWithToken(
    @CurrentUser() user: User,
    @Args('input') input: PersonInput,
  ) {
    if (!user) {
      throw new UnauthorizedException();
    }
    
    const createPersonResponse: IServiceGetPersonResponse = await this.userServiceClient
    .send('get_person_url_with_token', user.id)
    .toPromise();
    if (createPersonResponse.status !== HttpStatus.OK) {
      throw new HttpException(
        {
          message: createPersonResponse.message,
          data: null,
        },
        createPersonResponse.status,
      );
    }
    const jwtPerson = createPersonResponse.person;
    if (
      jwtPerson.firstName === input.firstName &&
      jwtPerson.lastName === input.lastName &&
      jwtPerson.passport === input.passport
    ) {
      console.log("get_person_url_with_token");
      console.log(user.username);
      return (
        encodeURI(`${HOSTNAME}/graphql?query={getPersonWithToken(code:"`) +
        `${encrypt(user.username).toString()}` +
        encodeURI(`"){id,firstName,lastName,passport}}`)
      );
    } else {
      return 'Invalid owner info!';
    }
  }

  @Query(() => PersonType)
  @UseGuards(GqlAuthGuard)
  async getPersonWithToken(
    @CurrentUser() user: User,
    @Args('code') code: string,
  ) {
    if (!user) {
      throw new UnauthorizedException();
    }
    const createPersonResponse: IServiceGetPersonResponse = await this.userServiceClient
      .send('get_person', code)
      .toPromise();
      if (createPersonResponse.status !== HttpStatus.OK) {
        throw new HttpException(
          {
            message: createPersonResponse.message,
            data: null,
          },
          createPersonResponse.status,
        );
      }
      let person = createPersonResponse.person;
      person.id = decrypt(code);
      console.log("getPersonWithToken");
      console.log(person);
      const createPersonResponse2: IServiceGetPersonResponse = await this.userServiceClient
      .send('get_person', encrypt(user.id))
      .toPromise();
      if (createPersonResponse2.status !== HttpStatus.OK) {
        throw new HttpException(
          {
            message: createPersonResponse2.message,
            data: null,
          },
          createPersonResponse2.status,
        );
      }
      const jwtPerson = createPersonResponse.person;

    if (
      person &&
      person.firstName === jwtPerson.firstName &&
      person.lastName === jwtPerson.lastName &&
      person.passport === jwtPerson.passport
    ) {
      return person;
    } else {
      throw new Error('Unmatch owner info!');
    }
  }
}
