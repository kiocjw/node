import { Document } from 'mongoose';

export interface IPerson extends Document {
  id?: string;
  firstName: string;
  lastName: string;
  passport: string;
}
