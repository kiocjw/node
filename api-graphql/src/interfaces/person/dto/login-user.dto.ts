import { ID, Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class LoginUserDto {
  @Field(() => ID)
  id: string;
  @Field()
  readonly passport: string;
}
