import { IPerson } from '../person.interface';

export class CreateUserResponseDto {
  
  message: string;

  data: {
    user: IPerson;
    token: string;
  };

  errors: { [key: string]: any };
}
