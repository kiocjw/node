import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class CreateUserDto {
  @Field()
  readonly firstName: string;
  @Field()
  readonly lastName: string;
  @Field()
  readonly passport: string;
  @Field({ nullable: true })
  readonly email?: string;
  @Field({ nullable: true })
  readonly address?: string;
  @Field({ nullable: true })
  username?: string;
}