import { IPerson } from './person.interface';

export interface IServiceGetPersonResponse {
  status: number;
  message: string;
  person: IPerson | null;
 
}
