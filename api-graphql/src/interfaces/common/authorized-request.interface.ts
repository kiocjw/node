import { IPerson } from '../person/person.interface';

export interface IAuthorizedRequest extends Request {
  user?: IPerson;
}
