import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { ConfigService } from './services/config/config.service';
import { ClientProxyFactory } from '@nestjs/microservices';
import { AppService } from './app.service';
import { AuthService } from './auth/auth.service';
import { AuthModule } from './auth/auth.module';
import { PersonsModule } from './persons/persons.module';
import { GraphQLModule } from '@nestjs/graphql';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    GraphQLModule.forRoot({
      autoSchemaFile: 'schema.gql',
    }),
    MongooseModule.forRoot(
      'mongodb+srv://ts-api:ts12345@cluster0.bq6zp.mongodb.net/ts-api-mongodb?retryWrites=true&w=majority',
    ),
    AuthModule,
    PersonsModule,
  ],
  controllers: [AppController],
  providers: [
    AppService
  ],
  })
export class AppModule {}
