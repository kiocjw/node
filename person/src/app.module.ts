import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { MongooseModule } from '@nestjs/mongoose';
import { PersonController } from './person.controller';
import { PersonsModule } from './persons/persons.module';


@Module({
  imports: [
    GraphQLModule.forRoot({
      autoSchemaFile: 'schema.gql',
    }),
    MongooseModule.forRoot(
      'mongodb+srv://ts-api:ts12345@cluster0.bq6zp.mongodb.net/ts-api-mongodb?retryWrites=true&w=majority',
    ),
    PersonsModule,
  ],
  controllers: [PersonController],

})
export class AppModule {}
