import { Controller, HttpStatus, Inject } from '@nestjs/common';
import { MessagePattern, ClientProxy } from '@nestjs/microservices';
import { IPerson } from './interfaces/person.interface';
import { IServicePersonCreateResponse } from './interfaces/service-create-person-url-response-interface';
import { IServiceGetPersonResponse } from './interfaces/service-get-person-response.interface';
import { PersonsService } from './persons/persons.service';
import { decrypt, encrypt } from './encryption';

@Controller('user')
export class PersonController {
  constructor(
    private readonly personService: PersonsService
  ) {}

  @MessagePattern('get_person_url_with_token')
  public async getPersonUrlWithToken(passport: string): Promise<IServiceGetPersonResponse> {
    let result: IServiceGetPersonResponse;
    console.log("Passport:"+passport)
    if (passport) {
      const user = await this.personService.findByPassport(
        passport);

      if (user) {
        if (await user) {
          result = {
            status: HttpStatus.OK,
            message: 'get_person_url_with_token_success',
            person: user,
          };
        } else {
          result = {
            status: HttpStatus.NOT_FOUND,
            message: 'get_person_url_with_token_not_match',
            person: null,
          };
        }
      } else {
        result = {
          status: HttpStatus.NOT_FOUND,
          message: 'get_person_url_not_found',
          person: null,
        };
      }
    } else {
      result = {
        status: HttpStatus.NOT_FOUND,
        message: 'get_person_url_not_found',
        person: null,
      };
    }

    return result;
  }

  @MessagePattern('get_person')
  public async getPerson(code: string): Promise<IServiceGetPersonResponse> {
    let result: IServiceGetPersonResponse;
    let id = decrypt(code);
    console.log("ID:"+ id)
    if (id) {
      let person = await this.personService.findByID(id);
      person.id = id;
      console.log(person);
      if (person) {
        result = {
          status: HttpStatus.OK,
          message: 'get_person_success',
          person:person,
        
        };
      } else {
        result = {
          status: HttpStatus.NOT_FOUND,
          message: 'get_person_not_found',
          person: null,
          
        };
      }
    } else {
      result = {
        status: HttpStatus.BAD_REQUEST,
        message: 'get_person_bad_request',
        person: null,
     
      };
    }

    return result;
  }
  @MessagePattern('create_person_url')
  public async createPersonURL(personParams: IPerson): Promise<IServicePersonCreateResponse> {
    let result: IServicePersonCreateResponse;

    if (personParams) {
      const usersWithPassport = await this.personService.findByPassport(personParams.passport,
      );

      if (usersWithPassport && usersWithPassport.passport.length > 0) {
        result = {
          status: HttpStatus.CONFLICT,
          message: 'user_create_conflict',
          code: null,
          errors: {
            passport: {
              message: 'Passport already exists',
              path: 'passport',
            },
          },
        };
      } else {
        try {
          const createdUser = await this.personService.create(personParams);
          result = {
            status: HttpStatus.CREATED,
            message: 'user_create_success',
            code: createdUser.id,
            errors: null,
          };
        } catch (e) {
          result = {
            status: HttpStatus.PRECONDITION_FAILED,
            message: 'user_create_precondition_failed',
            code: null,
            errors: e.errors,
          };
        }
      }
    } else {
      result = {
        status: HttpStatus.BAD_REQUEST,
        message: 'user_create_bad_request',
        code: null,
        errors: null,
      };
    }

    return result;
  }
}
