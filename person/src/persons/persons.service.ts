import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Person, PersonDocument } from './persons.schema';
import { PersonInput } from './inputs/person.input';
import { IPerson } from 'src/interfaces/person.interface';

@Injectable()
export class PersonsService {
  constructor(
    @InjectModel(Person.name) private personModel: Model<PersonDocument>,
  ) {}

  async create(createPersonDto: PersonInput): Promise<IPerson> {
    const createdPerson = new this.personModel(createPersonDto);
    return createdPerson.save();
  }

  async createurl(createPersonDto: PersonInput): Promise<string> {
    const createdPerson = new this.personModel(createPersonDto);
    return (await createdPerson.save()).id;
  }

  async findByID(ID: string): Promise<IPerson> {
    return this.personModel.findById(ID).exec();
  }

  async findByPassport(pass: string): Promise<IPerson> {
    return this.personModel.findOne({ passport: pass }).exec();
  }

  async findOne(ID: string): Promise<Person | undefined> {
    return this.personModel.findById(ID).exec();
  }

  async findAll(): Promise<Person[]> {
    return this.personModel.find().exec();
  }
}
