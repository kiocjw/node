import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';
import { UnauthorizedException, UseGuards } from '@nestjs/common';
import { PersonsService } from './persons.service';

@Resolver()
export class PersonsResolver {
  constructor(private readonly personsService: PersonsService) {}

  @Query(() => String)
  async index() {
    return 'Index';
  }
}
