import { NestFactory } from '@nestjs/core';
import { Transport, TcpOptions } from '@nestjs/microservices';

import { ConfigService } from './services/config/config.service';
import { AppModule } from './app.module';

async function bootstrap() {

  const codeFirstSchemaDummy = await NestFactory.create(AppModule);
  await codeFirstSchemaDummy.listen(1000);
  codeFirstSchemaDummy.close();
  
  const app = await NestFactory.createMicroservice(AppModule, {
    transport: Transport.TCP,
    options: {
      host: '0.0.0.0',
      port: new ConfigService().get('port'),
    },
  } as TcpOptions);
  await app.listenAsync();
}
bootstrap();
