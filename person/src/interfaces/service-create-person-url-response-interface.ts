export interface IServicePersonCreateResponse {
  status: number;
  message: string;
  code: string | null;
  errors: { [key: string]: any };
}
