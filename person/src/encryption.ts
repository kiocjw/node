import * as cryptojs from 'crypto-js';

export const KEY = 'secret key 123';

export const encrypt = (data: string) => {
  // Encrypt
  //console.log("input1:"+data);
  let ciphertext = cryptojs.AES.encrypt(data, KEY).toString();
  //console.log("input2:"+ciphertext);
  let re = /\+/gi;
  ciphertext = ciphertext.replace(re, 'xMl3Jk');
  re = /\//gi;
  ciphertext = ciphertext.replace(re, 'Por21Ld');
  re = /\=/gi;
  ciphertext = ciphertext.replace(re, 'Ml32');
  //console.log("input3:"+ciphertext);

  return ciphertext;
};

export const decrypt = (data: string) => {
  //console.log("output1:"+data);
  let ciphertext = data;
  let re = /xMl3Jk/gi;
  ciphertext = ciphertext.replace(re, '+');
  re = /Por21Ld/gi;
  ciphertext = ciphertext.replace(re, '/');
  re = /Ml32/gi;
  ciphertext = ciphertext.replace(re, '=');
  //console.log("output2:"+ciphertext);
  const bytes = cryptojs.AES.decrypt(ciphertext, KEY);
  const originalText = bytes.toString(cryptojs.enc.Utf8);
  //console.log("output3:"+originalText);

  return originalText;
};
