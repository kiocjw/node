import {
  MongooseOptionsFactory,
  MongooseModuleOptions,
} from '@nestjs/mongoose';

export class MongoConfigService implements MongooseOptionsFactory {
  createMongooseOptions(): MongooseModuleOptions {
    return {
      uri: "mongodb+srv://ts-api:ts12345@cluster0.bq6zp.mongodb.net/ts-api-mongodb",
    };
  }
}
